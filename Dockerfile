FROM alpine:latest

RUN apk --no-cache add ca-certificates lftp openssh-client && \
    lftp --version
